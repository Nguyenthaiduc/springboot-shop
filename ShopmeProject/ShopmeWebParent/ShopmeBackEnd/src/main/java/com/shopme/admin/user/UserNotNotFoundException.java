package com.shopme.admin.user;

public class UserNotNotFoundException extends Exception {

	public UserNotNotFoundException(String message) {
		super(message);
		
	}

}
