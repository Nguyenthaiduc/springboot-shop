package com.shopme.admin.user;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import com.shopme.common.entity.Role;
import com.shopme.common.entity.User;

@DataJpaTest(showSql = false)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {
	@Autowired
	private UserRepository repo;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	public void testCreateNewUserWithOneRole() {
		// id 1 la quyen admin
		Role roleAdmin = entityManager.find(Role.class, 1);
		User userDucNT = new User("ntd2@gmail.com", "12345678", "Duc", "Nguyen");
		userDucNT.addRole(roleAdmin);
		
		User savedUser = repo.save(userDucNT);

		assertThat(savedUser.getId()).isGreaterThan(0);
	}

	@Test
	public void testCreateNewUserWithTwoRoles() {
		User userXuan = new User("xuan@gmail.com", "123456", "Xuan", "Nguyen");
		Role roleEditor = new Role(3);
		Role roleAssistant = new Role(5);

		userXuan.addRole(roleEditor);
		userXuan.addRole(roleAssistant);

		User savedUser = repo.save(userXuan);

		assertThat(savedUser.getId()).isGreaterThan(0);

	}

	@Test
	public void testListAllUsers() {
		Iterable<User> listUsers = repo.findAll();
		listUsers.forEach(user -> System.out.println(user));
		assertThat(listUsers);

	}

	@Test
	public void testGetUserById() {
		User userDuc = repo.findById(1).get();
		System.out.println(userDuc);
		assertThat(userDuc).isNotNull();
	}

	@Test
	public void testUpdateUserDetails() {
		User userDuc = repo.findById(13).get();
		userDuc.setEnabled(true);
		userDuc.setEmail("ntd@hmail.com");

		repo.save(userDuc);
	}

	@Test
	public void testUpdateUserRoles() {
		User userXuan = repo.findById(4).get();
		Role roleEditor = new Role(3);
		Role roleSalesPerson = new Role(1);

		userXuan.getRoles().remove(roleEditor);
		userXuan.addRole(roleSalesPerson);

		repo.save(userXuan);
	}

	@Test
	public void testDeleteUser() {
		Integer userId = 2;
		repo.deleteById(userId);

	}

	@Test
	public void testGetUserByEmail() {
		String email = "azul@gmail.com";
		User user = repo.getUserByEmail(email);

		assertThat(user).isNotNull();
	}

	@Test
	public void testCountById() {
		Integer id = 1;
		Long countById = repo.countById(id);

		assertThat(countById).isNotNull().isGreaterThan(0);
	}

	@Test
	public void testDisabledUser() {
		Integer id = 1;
		repo.updateEnabledStatus(id, true);
	}

	
	@Test
	public void testListFirstPage() {
		int pageNumber = 0;
		int pageSize = 4;
		
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<User> page = repo.findAll(pageable);
		
		List<User> listUser = 	page.getContent();
		
		listUser.forEach(user -> System.out.println(user));
		
		assertThat(listUser.size()).isEqualTo(pageSize);
	}
	
	@Test
	public void testSearchUser() {
		String keyword = "duc";
		int pageNumber = 0;
		int pageSize = 4;
		
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<User> page = repo.findAll(keyword,pageable);
		
		List<User> listUser = 	page.getContent();
		
		listUser.forEach(user -> System.out.println(user));
		
		assertThat(listUser.size()).isGreaterThan(0);
		
	}
	

}
